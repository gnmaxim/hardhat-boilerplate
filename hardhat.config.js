require('@nomiclabs/hardhat-waffle');

require('dotenv').config();

require('./tasks/accounts');
require('./tasks/balance');
require('./tasks/block-number');

module.exports = {
    roles: {
        deployer: {
            default: 0, // first account as deployer
            1: 0, // similarly on mainnet it will take the first account as deployer
        },
    },

    solidity: {
        version: '0.8.4',
        settings: {
            optimizer: {
                enabled: true,
                runs: 200,
            },
        },
    },

    paths: {
        sources: './contracts',
        tests: './test',
        cache: './cache',
        artifacts: './artifacts',
    },

    mocha: {
        timeout: 91000,
    },

    defaultNetwork: 'hardhat',

    networks: {
        localhost: {
            url: 'http://127.0.0.1:7545'
        },

        ethereum: {
            url: process.env.ETH_MAINNET_RPC,
            accounts: [process.env.ETH_MAINNET_PRIV],
        },        

        rinkeby: {
            url: process.env.ETH_RINKEBY_RPC_URL,
            accounts: [process.env.ETH_RINKEBY_PRIV]
        },
        
        polygon: {
            url: process.env.POLYGON_MAINNET_RPC,
            accounts: [process.env.POLYGON_MAINNET_PRIV],
        },

        mumbai: {
            url: process.env.POLYGON_MUMBAI_RPC,
            accounts: [process.env.POLYGON_MUMBAI_PRIV],
        }
    },

    etherscan: {
        apiKey: {
            mainnet: process.env.ETH_MAINNET_ETHERSCAN_API_KEY,
            rinkeby: process.env.ETH_RINKEBY_ETHERSCAN_API_KEY,
            polygon: process.env.POLYGON_MAINNET_POLYGONSCAN_API_KEY,
            polygonMumbai: process.env.POLYGON_MUMBAI_POLYGONSCAN_API_KEY,
        }
    },
};
